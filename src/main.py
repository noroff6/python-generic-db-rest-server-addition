from flask import Flask, request, Response
from flask_cors import CORS
import json

from create_db import db_init, Data

ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif'}

### FLASK LOGIC
app = Flask(__name__)
CORS(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///../data.db'
db = db_init(app)

@app.route("/")
def index():
    return f'Generic database, serving {db.session.query(Data).count()} entries.'

@app.route("/submit", methods=['POST'])
def submit():
    # Generate meta data
    _owner = request.environ['HTTP_HOST'] + "::" + request.environ['HTTP_USER_AGENT']

    # Fetch json data from message
    post_json = request.get_json()
    if post_json is None: return Response(status=400,response="POST content must contain a JSON object")

    # Fetch data from json
    data = post_json.get("data",None)
    meta_data = post_json.get("meta",None)
    pokemon_id = post_json.get("pokemon_id", None);
    if data is None: return Response(status=400,response="Request JSON object must contain 'data'")
    if meta_data is None: return Response(status=400,response="Request JSON object must contain 'meta'")
    if pokemon_id is None: return Response(status=400,response="Request JSON object must contain 'pokemon_id'")
    
    
    # Create Data row
    new_row = Data(owner=_owner, data=data, meta_data=meta_data, pokemon_id=pokemon_id)

    # Push to DB
    try:
        db.session.add(new_row)
        db.session.commit()
        return Response(status=201, response="Data entry created")
    except Exception as e:
        return Response(status=500, response=str(e))

@app.route("/fetch", methods=['GET','POST'])
def fetch():
    rows = None
    if request.method == "GET":
        # Generate meta data
        _owner = request.environ['HTTP_HOST'] + "::" + request.environ['HTTP_USER_AGENT']

        # Rows from owner
        rows = db.session.query(Data).filter(Data.owner == _owner).all()

    else:
        # Fetch username from post
        post_json = request.get_json()
        if post_json is None: return Response(status=400,response="POST content must contain a JSON object")

        # Fetch data from json
        user = post_json.get("user",None)
        if user is None: return Response(status=400,response="Request JSON object must contain 'data'")

        users = [x["owner"] for x in db.session.query(Data.owner).distinct().all()]
        if user not in users: 
            return Response(status=404,response=f'''User "{user}" not found in owner column.''')
        else:
            # Rows from owner
            rows = db.session.query(Data).filter(Data.owner == user).all()

    # Create response
    return Response(
            status=200, 
            response = json.dumps([row.dict() for row in rows]),
            mimetype='application/json'
        )

@app.route("/fetch/all")
def fetch_all():
    # All rows
    rows = db.session.query(Data).all()
    
    # Create response
    return Response(
            status=200, 
            response = json.dumps([row.dict() for row in rows]),
            mimetype='application/json'
        ) 

@app.route("/fetch/<int:id>")
def fetch_by_id(id):
    #Find row
    row = db.session.query(Data).filter_by(id=id).first()

    # Send 404 if not found
    if not row: Response(status=404,response=f'''No data with id "{id}" found.''')

    # Otherwise send data back
    return Response(
            status=200, 
            response = row.json(),
            mimetype='application/json'
        )

@app.route("/fetch/pokemon/<int:pokemon_id>")
def fetch_by_pokemon_id(pokemon_id):
    # Find row
    row = db.session.query(Data).filter_by(pokemon_id=pokemon_id).first()

    # Send 404/500 if not found
    #if not row: return Response(status=404,response=f'''No data with id "{pokemon_id}" found.''')
    if not row: return Response(status=404,response="")
    if not row: return Response(status=500,response="")

    # Otherwise send data back
    return Response(
            status=200, 
            response = row.json(),
            mimetype='application/json'
        )

@app.route("/upload", methods=['POST'])
def upload():
    # extract image
    image = request.files["image"]

    # Ensure file is there
    if image is None: 
        return Response(status=400,response="POST request must contain file labeled 'image'")

    if image.filename.split(".")[-1].lower() not in ALLOWED_EXTENSIONS:
        return Response(status=400,response="File extention not supported. Must be an image.")

    # Extract meta data
    _owner = request.environ['HTTP_HOST'] + "::" + request.environ['HTTP_USER_AGENT']
    _meta = image.mimetype
    new_row = Data(owner=_owner, data = image.read(), meta_data=_meta)

    # Push to DB
    try:
        db.session.add(new_row)
        db.session.commit()
        return Response(status=201, response="Image uploaded successfully.")
    except Exception as e:
        return Response(status=500, response=str(e))

if __name__ == '__main__':
    app.run(debug=True)
