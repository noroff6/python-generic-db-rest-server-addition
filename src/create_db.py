from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
import json
import base64

db = SQLAlchemy()

class Data(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    owner = db.Column(db.Text, nullable=False)
    date_created = db.Column(db.DateTime, default=datetime.utcnow)
    data = db.Column(db.Text, nullable=False)
    meta_data = db.Column(db.Text, nullable=False)
    pokemon_id = db.Column(db.Integer, nullable=False)
    def __repr__(self) -> str:
        return f"<Data item id: {self.id}>"
    def __str__(self) -> str:
        return f"<Data item id: {self.id} from {self.owner} on {self.date_created}. Length: {len(self.data)} Meta:{self.meta_data} PokemonId:{self.pokemon_id}>"
    def json(self):
        _data = self.data
        try:
            json.dumps(_data)
        except TypeError:
            _data = base64.encodebytes(_data).decode("utf-8")
        return json.dumps( {
            "id": self.id,
            "owner" : self.owner,
            "date_created": str(self.date_created),
            "data": _data,
            "meta_data": self.meta_data,
            "pokemon_id": self.pokemon_id
        })
    def dict(self):
        _data = self.data
        try:
            json.dumps(_data)
        except TypeError:
            _data = base64.encodebytes(_data).decode("utf-8")
        return {
            "id": self.id,
            "owner" : self.owner,
            "date_created": str(self.date_created),
            "data": _data,
            "meta_data": self.meta_data,
            "pokemon_id": self.pokemon_id
        }

def db_init(app):
    global db
    db.init_app(app)
    with app.app_context():
        db.create_all()
    return db